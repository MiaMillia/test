# Bibliotheken zur Überwachung von Systemressourcen und Verbindung mit der PostgreSQL-Datenbank importieren
import psutil
import psycopg2
from datetime import datetime
import time


# Funktion zum Speichern der Ressourcenwerte in der Datenbank
def save_resource_values(timestamp, host_name, resource_name, state_value):
    # Verbindung zur PostgreSQL-Datenbank herstellen
    conn = psycopg2.connect(database="system_monitoring_database", user="postgres", password="123456", host="localhost",
                            port="5432")
    cur = conn.cursor()
    # SQL-Abfrage ausführen
    cur.execute(
        "INSERT INTO monitoring_data (timestamp, host_name, resource_name, state_value) VALUES (%s, %s, %s, %s)",
        (timestamp, host_name, resource_name, state_value))
    conn.commit()
    # Verbindung zur Datenbank trennen
    cur.close()
    conn.close()


# Funktion zum Überwachen und Speichern der Ressourcenwerte
def monitor_and_save_resources():
    timestamp = datetime.now()
    host_name = "MPC-53230"

    # Überwachung der CPU
    cpu_usage = psutil.cpu_percent()
    save_resource_values(timestamp, host_name, "CPU", cpu_usage)

    # Überwachung der Festplatte
    disk_usage = psutil.disk_usage('/').percent
    save_resource_values(timestamp, host_name, "Festplatte", disk_usage)

    # Überwachung des Arbeitsspeichers
    memory_usage = psutil.virtual_memory().percent
    save_resource_values(timestamp, host_name, "Arbeitsspeicher", memory_usage)

    print("Ressourcenwerte erfolgreich gespeichert.")


# Schleife zum periodischen Überwachen und Speichern der Ressourcenwerte
while True:
    monitor_and_save_resources()
    time.sleep(3600)  # Verzögerung von einer Stunde (3600 Sekunden)
